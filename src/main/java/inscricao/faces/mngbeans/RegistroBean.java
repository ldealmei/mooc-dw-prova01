/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Login;
import java.util.ArrayList;
import javax.inject.Named;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;

/**
 *
 * @author Lucas
 */
@Named(value = "registroBean")
@ManagedBean
@ApplicationScoped
public class RegistroBean extends utfpr.faces.support.PageBean {

    private ArrayList<Login> loginsList;

    public ArrayList<Login> getLoginsList() {
        return loginsList;
    }

    public void setLoginsList(ArrayList<Login> loginsList) {
        this.loginsList = loginsList;
    }

    
    /**
     * Creates a new instance of RegistroBean
     */
    public RegistroBean() {
        loginsList = new ArrayList<Login>();
    }

    public void addLogin(Login login) {
        loginsList.add(login);
    }
    
    
}
