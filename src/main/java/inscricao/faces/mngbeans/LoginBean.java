/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import inscricao.entity.Login;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.enterprise.context.ApplicationScoped;
import javax.inject.Named;
import javax.enterprise.context.Dependent;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.validator.ValidatorException;

/**
 *
 * @author Lucas
 */
@Named(value = "loginBean")
@ManagedBean
@ApplicationScoped
public class LoginBean extends utfpr.faces.support.PageBean{

    private String login = "";
    private String senha = "";
    private boolean administrador = false;
    private String result = "";
    private Date hora;

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public boolean isAdministrador() {
        return administrador;
    }

    public void setAdministrador(boolean administrador) {
        this.administrador = administrador;
    }

    /**
     * Creates a new instance of LoginBean
     */
    public LoginBean() {
    }

    public String fazerLogin() {
        if (login.equals(senha)) {
            //return true;
            result = "";
            hora = new Date();
            Login l = new Login();
            l.setData(new Date());
            l.setLogin(login);
            RegistroBean bean = (RegistroBean) getBean("registroBean");
            bean.addLogin(l);
            if (administrador){
                return "admin";
            }
        }
        result = "Acesso negado";
        return null;
        //return false;
//        FacesMessage message = new FacesMessage("Acesso negado");
//        throw new ValidatorException(message);
    }

    public Date getHora() {
        return hora;
    }

    public String getStringHora() {
        DateFormat formato = new SimpleDateFormat("HH:mm:ss");
        String res = formato.format(hora);
        return res;
    }
}
